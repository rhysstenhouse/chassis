# About
Chassis is a simple CSS grid framework for quickly creating webpage layouts. It’s designed to be as lightweight, flexible and semantic-friendly as possible.

### Syntax
The framework relies on a row and column system that you’ll probably already have seen in other CSS frameworks:
* <code>\<div class="row"></code>
* <code>\<div class="small-10 med-8 large-6 col"></code>

### Contribute
Fresh eyes are always a big help, so please feel free to get involved by submitting issues or pull requests (please note that development work is normally done on the dev branch).

Check out the [Roadmap](https://github.com/rhysdoesdesign/Chassis/issues/1) if you want some ideas on where I want to take Chassis.

### License
Chassis is open source and free to use under an MIT license. If you use it in a cool project I’d love to hear about it!



## Features
* Column-based grid with easily customisable column numbers and margins
* Three (customisable) breakpoints for small, medium and large screens
* Built-in offset classes for whitespace without spacer <code>\<div></code>s


## Credits
* Inspired by [Foundation](http://foundation.zurb.com)
* Built on the (now defunct) [1140px Grid](https://github.com/andytlr/cssgrid). (See the original fork [here](https://github.com/rhysdoesdesign/chassis-old))
* Built with [Brackets](http://brackets.io) and [Sass](http://sass-lang.com).