## Grid 
The default grid measurements are as follows:

* 12 columns
* 1 column = 4.85%
* margins = 3.8%

These are defined in _gridvars.scss; if you’re using Sass then you can change the grid measurements on the fly. I recommend picking numbers which will add up to a round 100%, eg:

> 12 columns = (4.85 * 12) + (3.8 * 11) = 58.2 + 41.8 = 100

Note that the margins value represents the CSS space between column content, **including margins and padding**. The Sass variables deal with this automatically like so:

* <code>$column-unit</code> (column content) = 4.85%
* <code>$margin-width</code> (gutter space) = 3.8%
* <code>$margin-unit</code> (CSS margin-right on column elements) = <code>$margin-width</code> / 2
* <code>$padding-unit</code> (total of CSS padding-left and padding-right on column elements) = [<code>$margin-unit</code> * (number of margins)] / (number of columns * 2) / 2


## Adding more columns
### With Sass
If you want a grid with more (or less) columns, you can just edit the <code>$column-count</code> and <code>$margin-width</code> variables in _gridvars.scss.

### Without Sass
If you’re not using Sass, you’ll need to manually add more of the following classes:

#### Small breakpoint:
* .small-n
* .small-offset-n
    
#### Medium breakpoint:
* .med-n
* .med-offset-n
    
#### Large breakpoint:
* .large-n
* .large-offset-n
    
You’ll also need to adjust the various units in the CSS to ensure your grid rows will still fit together.